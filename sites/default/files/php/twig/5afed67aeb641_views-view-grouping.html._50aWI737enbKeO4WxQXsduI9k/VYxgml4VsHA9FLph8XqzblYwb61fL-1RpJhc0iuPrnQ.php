<?php

/* core/themes/stable/templates/views/views-view-grouping.html.twig */
class __TwigTemplate_30df38032f4546285598ce9151501d3a53c5429316ea1f6842d072278c0eeda1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 17
        echo "<div class=\"grouper\">
    <div class=\"navigator\">
    <a class=\"next\"></a>
        <div class=\"group-title\"><h2>";
        // line 20
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</h2></div>
    <a class=\"prev\"></a>
    </div>
";
        // line 23
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["content"] ?? null), "html", null, true));
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "core/themes/stable/templates/views/views-view-grouping.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 23,  48 => 20,  43 => 17,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
/**
 * @file
 * Theme override to display a single views grouping.
 *
 * Available variables:
 * - view: The view object.
 * - grouping: The grouping instruction.
 * - grouping_level: A number indicating the hierarchical level of the grouping.
 * - title: The group heading.
 * - content: The content to be grouped.
 * - rows: The rows returned from the view.
 *
 * @see template_preprocess_views_view_grouping()
 */
#}
<div class=\"grouper\">
    <div class=\"navigator\">
    <a class=\"next\"></a>
        <div class=\"group-title\"><h2>{{ title }}</h2></div>
    <a class=\"prev\"></a>
    </div>
{{ content }}
</div>", "core/themes/stable/templates/views/views-view-grouping.html.twig", "C:\\MAMP\\htdocs\\apresto1\\core\\themes\\stable\\templates\\views\\views-view-grouping.html.twig");
    }
}
