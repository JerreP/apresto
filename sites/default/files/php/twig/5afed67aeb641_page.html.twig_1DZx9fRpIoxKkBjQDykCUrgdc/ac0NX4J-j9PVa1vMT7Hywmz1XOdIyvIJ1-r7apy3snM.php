<?php

/* themes/wundertheme-8.x/templates/layout/page.html.twig */
class __TwigTemplate_292af8efce43fccdac5777b37ee9f6f817ae9fdcb959c06b04c3467e99398d2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 51);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 49
        echo "
<div role=\"document\" class=\"page\">
  ";
        // line 51
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 52
            echo "    <header id=\"site-header\">
      <div class=\"outer-wrapper\">
        ";
            // line 54
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
            echo "
      </div>
    </header>
  ";
        }
        // line 58
        echo "
  ";
        // line 59
        if (($context["messages"] ?? null)) {
            // line 60
            echo "    <section id=\"messages\">
      <div class=\"outer-wrapper\">
        ";
            // line 62
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["messages"] ?? null), "html", null, true));
            echo "
      </div>
    </section>
  ";
        }
        // line 66
        echo "
  ";
        // line 67
        if (($context["breadcrumb"] ?? null)) {
            // line 68
            echo "    <section id=\"breadcrumb\">
      <div class=\"outer-wrapper\">
        ";
            // line 70
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["breadcrumb"] ?? null), "html", null, true));
            echo "
      </div>
    </section>
  ";
        }
        // line 74
        echo "
  <main role=\"main\" class=\"outer-wrapper\">
    ";
        // line 76
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())) {
            // line 77
            echo "      <aside id=\"sidebar-first\" role=\"complementary\" class=\"sidebar\">
        ";
            // line 78
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_first", array()), "html", null, true));
            echo "
      </aside>
    ";
        }
        // line 81
        echo "
    ";
        // line 82
        if ($this->getAttribute(($context["page"] ?? null), "left_top_content", array())) {
            // line 83
            echo "    <div id=\"top-content-wrapper\"><div id=\"left-top-content\">
      ";
            // line 84
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "left_top_content", array()), "html", null, true));
            echo "
    </div>
    ";
        }
        // line 87
        echo "
    ";
        // line 88
        if ($this->getAttribute(($context["page"] ?? null), "right_top_content", array())) {
            // line 89
            echo "    <div id=\"right-top-content\">
      ";
            // line 90
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "right_top_content", array()), "html", null, true));
            echo "
    </div></div>
    ";
        }
        // line 93
        echo "    
    ";
        // line 94
        if ($this->getAttribute(($context["page"] ?? null), "content", array())) {
            // line 95
            echo "    <section id=\"content\">
      ";
            // line 96
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo "
    </section>
    ";
        }
        // line 99
        echo "
    ";
        // line 100
        if ($this->getAttribute(($context["page"] ?? null), "frontpage_content", array())) {
            // line 101
            echo "    <div id=\"frontpage-content\">
      ";
            // line 102
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "frontpage_content", array()), "html", null, true));
            echo "
    </div>
    ";
        }
        // line 105
        echo "
    ";
        // line 106
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())) {
            // line 107
            echo "      <aside id=\"sidebar-second\" role=\"complementary\" class=\"sidebar\">
        ";
            // line 108
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()), "html", null, true));
            echo "
      </aside>
    ";
        }
        // line 111
        echo "  </main>

  ";
        // line 113
        if ($this->getAttribute(($context["page"] ?? null), "footer", array())) {
            // line 114
            echo "    <footer id=\"site-footer\" role=\"contentinfo\">
        <section class=\"footer\">
          ";
            // line 116
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer", array()), "html", null, true));
            echo "
        </section>
    </footer>
  ";
        }
        // line 120
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/wundertheme-8.x/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 120,  187 => 116,  183 => 114,  181 => 113,  177 => 111,  171 => 108,  168 => 107,  166 => 106,  163 => 105,  157 => 102,  154 => 101,  152 => 100,  149 => 99,  143 => 96,  140 => 95,  138 => 94,  135 => 93,  129 => 90,  126 => 89,  124 => 88,  121 => 87,  115 => 84,  112 => 83,  110 => 82,  107 => 81,  101 => 78,  98 => 77,  96 => 76,  92 => 74,  85 => 70,  81 => 68,  79 => 67,  76 => 66,  69 => 62,  65 => 60,  63 => 59,  60 => 58,  53 => 54,  49 => 52,  47 => 51,  43 => 49,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
/**
 * @file
 * Default theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - messages: Status and error messages. Should be displayed prominently.
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.primary_menu: Items for the primary menu region.
 * - page.secondary_menu: Items for the secondary menu region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 * - page.breadcrumb: Items for the breadcrumb region.
 * - azg_logo: The AZG logo in the site language
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 *
 * @ingroup themeable
 */
#}

<div role=\"document\" class=\"page\">
  {%  if page.header %}
    <header id=\"site-header\">
      <div class=\"outer-wrapper\">
        {{ page.header }}
      </div>
    </header>
  {% endif %}

  {% if messages %}
    <section id=\"messages\">
      <div class=\"outer-wrapper\">
        {{ messages }}
      </div>
    </section>
  {% endif %}

  {% if breadcrumb %}
    <section id=\"breadcrumb\">
      <div class=\"outer-wrapper\">
        {{ breadcrumb }}
      </div>
    </section>
  {% endif %}

  <main role=\"main\" class=\"outer-wrapper\">
    {% if page.sidebar_first %}
      <aside id=\"sidebar-first\" role=\"complementary\" class=\"sidebar\">
        {{ page.sidebar_first }}
      </aside>
    {% endif %}

    {% if page.left_top_content  %}
    <div id=\"top-content-wrapper\"><div id=\"left-top-content\">
      {{ page.left_top_content }}
    </div>
    {% endif %}

    {% if page.right_top_content  %}
    <div id=\"right-top-content\">
      {{ page.right_top_content }}
    </div></div>
    {% endif %}
    
    {% if page.content %}
    <section id=\"content\">
      {{ page.content }}
    </section>
    {% endif %}

    {% if page.frontpage_content %}
    <div id=\"frontpage-content\">
      {{ page.frontpage_content }}
    </div>
    {% endif %}

    {% if page.sidebar_second %}
      <aside id=\"sidebar-second\" role=\"complementary\" class=\"sidebar\">
        {{ page.sidebar_second }}
      </aside>
    {% endif %}
  </main>

  {% if page.footer %}
    <footer id=\"site-footer\" role=\"contentinfo\">
        <section class=\"footer\">
          {{ page.footer }}
        </section>
    </footer>
  {% endif %}
</div>
", "themes/wundertheme-8.x/templates/layout/page.html.twig", "C:\\MAMP\\htdocs\\apresto1\\themes\\wundertheme-8.x\\templates\\layout\\page.html.twig");
    }
}
