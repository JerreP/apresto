<?php

/* modules/apresto_general/templates/apresto-boolean.html.twig */
class __TwigTemplate_b1f815ba29a26e726794f535cffcba70fb56d159d088ac3b07c281afd0e040b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        if (($context["selected"] ?? null)) {
            // line 2
            echo "  <div class=\"selected\"><div class=\"outer-circle\"><div class=\"inner-circle\"></div></div></div>
";
        } else {
            // line 4
            echo "  <div class=\"unselected\"><div class=\"outer-circle\"></div></div>
";
        }
    }

    public function getTemplateName()
    {
        return "modules/apresto_general/templates/apresto-boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  45 => 2,  43 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if selected %}
  <div class=\"selected\"><div class=\"outer-circle\"><div class=\"inner-circle\"></div></div></div>
{% else %}
  <div class=\"unselected\"><div class=\"outer-circle\"></div></div>
{% endif %}", "modules/apresto_general/templates/apresto-boolean.html.twig", "C:\\MAMP\\htdocs\\apresto1\\modules\\apresto_general\\templates\\apresto-boolean.html.twig");
    }
}
