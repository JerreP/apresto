<?php

namespace Drupal\apresto_general;

use Drupal\Component\Utility\SortArray;

class AllergenenController{

  /**
   * Custom page for allergenenlijst
   */
  public function build() {
    // Gets all belegde broodjes
    $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => 'Belegde broodjes', 'vid' => 'menu_categorie']);
    $term = reset($term);
    $broodjes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => 'voeding', 'field_menu' => $term->id()]);

    // Get all allergenen
    $allergenen = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'allergenenlijst']);

    // Sort by title of allergenen
    uasort($allergenen, [SortArray::class, 'sortByTitleElement']);
    $rows = [];
    $headers = ['Belegde broodjes'];

    // Allergenen as headers
    foreach ($allergenen as $allergeen) {
      $headers[] = $allergeen->label();
    }
    // Loop over broodjes and check if allergeen is selected or not.
    foreach ($broodjes as $broodje) {
      $row = [$broodje->label()];
      $selected_allergenen = $broodje->get('field_allergenenlijst')->referencedEntities();
      $selected = [];
      if (!empty($selected_allergenen)) {
        foreach ($selected_allergenen as $item) {
          $selected[$item->id()] = $item->label();
        }
      }
      foreach ($allergenen as $allergeen) {
        $col = [
          '#theme' => 'apresto_boolean',
          '#selected' => FALSE
        ];
        if (key_exists($allergeen->id(), $selected)) {
          $col['#selected'] = TRUE;
        }
        $row[] = \Drupal::service('renderer')
          ->render($col);
      }
      $rows[] = $row;
    }
    return [
      '#theme' => 'table__allergenen',
      '#header' => $headers,
      '#rows' => $rows
    ];
  }
}