<?php

namespace Drupal\apresto_general\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Plugin implementation of the 'entity reference all boolean' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_boolean_all",
 *   label = @Translation("All Boolean"),
 *   description = @Translation("Display all the Referenced entities as boolean."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceAllBooleanFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Get type and bundles
    $settings = $this->fieldDefinition->getSettings();
    $type = $settings['target_type'];
    $bundles = $settings['handler_settings']['target_bundles'];

    $all_entities = [];
    foreach ($bundles as $bundle) {
      $entities = \Drupal::entityTypeManager()->getStorage($type)->loadByProperties(['vid' => $bundle]);
      $all_entities = array_merge($all_entities, $entities);
    }

    $selected_entities = $this->getEntitiesToView($items, $langcode);

    foreach ($all_entities as $delta => $entity) {
      $elements[$delta] = [
        '#theme' => 'apresto_boolean',
        '#selected' => in_array($entity, $selected_entities)
      ];
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

}
