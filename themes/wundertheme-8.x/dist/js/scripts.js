(function ($) {
  // Drupal.behaviors.contentSlider = {

    showSlides(0);
    jQuery('body').on('click', '.next', function() {
      var slides = jQuery('.grouper');
      var active = slides.filter(':visible');
      var index = slides.index(active);
      showSlides(index + 1);
    });
    jQuery('body').on('click', '.prev', function() {
      var slides = jQuery('.grouper');
      var active = slides.filter(':visible');
      var index = slides.index(active);
      showSlides(index - 1);
    });

    function showSlides(n, element) {
      var i = 0;
      
      var slides = document.getElementsByClassName("grouper");
      if (n >= slides.length) {n = 0}    
      if (n < 0) {n = slides.length - 1}
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";  
      }
      slides[n].style.display = "block";
      
    }

}(jQuery));